function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat: 47.3774497, lng: 8.5016957}
  });

    getLocation();

    var searchBox = new google.maps.places.SearchBox(document.getElementById('search-input'));
		searchBox.addListener('places_changed', function() {
      searchCoverage();
    }); 
}

function defaultMarker(){
  marker = new google.maps.Marker({
    position: {lat:47.3774497, lng:8.5016957},
    map: map
  });
}

function getLocation(){
  if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
  } else {
      defaultMarker();
  }
};


function successCallback(position){
    marker = new google.maps.Marker({
      position: {lat: position.coords.latitude, lng: position.coords.longitude},
      map: map
    });
    var position = position.coords.latitude + "," + position.coords.longitude;
    document.getElementById('search-input').value=position;
    searchCoverage();
};  

function errorCallback(error){
  defaultMarker();
};


// Fix layout
$('#map').height($('main').height())
$('#search').submit(false);
$(".dropdown-trigger").dropdown();
$(document).ready(function(){
    $('select').select();
  });



function searchCoverage() {
  M.toast({html: 'Finding coverage...'})
  $.ajax({
    "async": true,
    "crossDomain": true,
    "url": "https://coverage.api.simplecell.eu/get_coverage",
    "method": "POST",
    "headers": {
      "content-type": "application/x-www-form-urlencoded",
    },
    "data": {
      "key": "d6f1f41d-3c96-4a5e-b7cd-c0cc3607407b",
      "use_case": $("#use-case").val(),
      "coverage_type": "outdoor",
      "lang": "en",
      "address": $("#search-input").val()
    }
    }).done(function (responseOutdoor) {
    
      if (!responseOutdoor.hasOwnProperty("predicted_lat")) {
        M.toast({html: 'Error. Unable to find coordinates from given address.'})
        return false;
      } else {
        $.ajax({
          "async": true,
          "crossDomain": true,
          "url": "https://coverage.api.simplecell.eu/get_coverage",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
          },
          "data": {
            "key": "d6f1f41d-3c96-4a5e-b7cd-c0cc3607407b",
            "use_case": $("#use-case").val(),
            "coverage_type": "indoor",
            "lang": "en",
            "address": $("#search-input").val()
          }
          }).done(function (responseIndoor) {
            $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "https://coverage.api.simplecell.eu/get_coverage",
            "method": "POST",
            "headers": {
              "content-type": "application/x-www-form-urlencoded",
            },
            "data": {
              "key": "d6f1f41d-3c96-4a5e-b7cd-c0cc3607407b",
              "use_case": $("#use-case").val(),
              "coverage_type": "deep_indoor",
              "lang": "en",
              "address": $("#search-input").val()
            }
          }).done(function (responseUnderground) {

            if(typeof marker == "undefined"){
              defaultMarker();
            }
            // Map reposition
            var newLatLng = new google.maps.LatLng(responseOutdoor.predicted_lat, responseOutdoor.predicted_lng);
            marker.setPosition(newLatLng);
            map.setCenter(marker.getPosition());
            map.setZoom(15);

            // Infowindow
            if (typeof infowindow !== "undefined"){
              infowindow.close()
            }

            infowindow = new google.maps.InfoWindow({
                content: "<big>Coverage quality</big><br />Outdoor: <b>" + responseOutdoor.quality + "</b><br />Indoor: <b>" + responseIndoor.quality + "</b><br />Underground: <b>" + responseUnderground.quality + "</b>"
            });
            infowindow.open(map,marker);

            // Radius
            if (typeof radius !== "undefined"){
              radius.setMap(null)
            }
            radius = new google.maps.Circle({
              strokeColor: '#5558A6',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: '#5558A6',
              fillOpacity: 0.35,
              map: map,
              center: marker.getPosition(),
              radius: responseOutdoor.radius
            });
          });
        });
      }
  });
}


